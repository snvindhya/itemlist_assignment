import {combineReducers} from 'redux';
import itemReducer from './ItemReducer';

const rootReducer = combineReducers({list: itemReducer});

export default rootReducer;
