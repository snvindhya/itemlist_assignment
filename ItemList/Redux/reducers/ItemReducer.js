import {ADD_ITEM, REMOVE_ITEM} from '../actions/ItemAction/ActionTypes';

const INITIAL_STATE = {items: [
  {
    title:"Vindhya",
    description:"Hello world"
  }
]};

const itemReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_ITEM:
      return { 
        ...state,
        items: [...state.items, action.payload]
    }
    case REMOVE_ITEM:
      return {items: handleRemoveItem(action.payload, state.items)};
    default:
      return state;
  }
};

const handleRemoveItem = (item, items) => {
  const itemIndex = items.indexOf(item);
  items.splice(itemIndex, 1);
  return items;
};

export default itemReducer;
