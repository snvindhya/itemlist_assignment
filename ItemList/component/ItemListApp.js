import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AddItem, RemoveItem } from '../Redux/actions/ItemAction/ItemAction';
import { styles } from './ItemListStyles';
import { Text, View, Button, FlatList } from 'react-native';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import Modal from "react-native-modal";
import { Icon, Input, Header, SearchBar } from 'react-native-elements'
import { useEffect } from 'react';

const ItemList = () => {

  const [item, setItem] = useState({ title: "", description: "" });
  const [isModalVisible, setModalVisible] = useState(false);
  const [searchInput, setSearchInput] = useState(null);
 
  const dispatch = useDispatch();
  const data = useSelector(state => state);
  const [list,setList]= useState(data.list?.items);
  const [tempList,setTempList] = useState(data.list?.items);

 
  useEffect(() => {
    setList(data.list?.items)
 },[data])
 

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const addItem = () => {
     dispatch(AddItem(item));
     toggleModal() 
  }

  const removeItem = item => {
    const itemIndex = list.indexOf(item);
    if (itemIndex > -1) {
      dispatch(RemoveItem(item));
    } else {
      alert(`${itemIndex.title} is not in the List`);
    }
  };

  const searchItem = (value)=>{
    if ('' == searchInput) {
      setList(data.list?.items)
    }
    else {
      const newData = tempList.filter(item => {
        const itemData = `${item.title.toUpperCase()}   
          ${item.description.toUpperCase()}`;
        const textData = value.toUpperCase();
        return itemData.indexOf(textData) > -1;
      });
      setList(newData)
    }  
  }

  const RenderItemList = () => {
    return (
      <FlatList
        data={list}
        renderItem={({ index, item }) => (
          <View style={styles.listContainer}>
            <View style={styles.listView}>
              <View style={styles.textContainer}>
                <Text style={styles.title}>#{index + 1} Title: {item.title}</Text>
                <Text style={styles.description}>{item.description}</Text>
              </View>
              <Icon name='trash' color='red' type='ionicon' size={25} onPress={() => removeItem(item)} />
            </View>
          </View>
        )}
      />
    );
  };

  const HeaderComponet = () => {
    return <Header
      centerComponent={{ text: 'Items', style: { color: 'red', fontSize: 25, fontWeight: 'bold' } }}
      rightComponent={<Icon name='add' color='red' type='ionicon' size={30} onPress={toggleModal} />}
      containerStyle={{ backgroundColor: 'lightgray', justifyContent: 'space-around' }} />
  }
  return (
    <SafeAreaProvider>
      <HeaderComponet />
      <Modal isVisible={isModalVisible} backdropColor="gray" backdropOpacity={1}>
        <View style={styles.container}>
          <Input
            style={styles.inputStyle}
            placeholder='Add Title'
            placeholderTextColor="#FFF"
            leftIcon={{ type: 'font-awesome', name: 'book', color: 'white' }}
            onChangeText={title => {
              setItem({ ...item, title: title })
            }}
          />
          <Input
            style={styles.inputStyle}
            placeholder='Add description'
            placeholderTextColor="#FFF"
            leftIcon={{ type: 'font-awesome', name: 'pencil', color: 'white' }}
            onChangeText={value => {
              setItem({ ...item, description: value })
            }}
          />
          <Button name="increase" title="Add Item" onPress={addItem} />
        </View>
      </Modal>
      <SearchBar
        placeholder="Search Here..."
        lightTheme
        value={searchInput}
        onChangeText={value=>searchItem(value)}
        autoCorrect={false}
      />
      <RenderItemList />
    </SafeAreaProvider>
  );
};

export default ItemList;
