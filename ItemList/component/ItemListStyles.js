import {StyleSheet} from 'react-native';

const TodoStyles = {
  container: {
    flex: 1, alignItems: 'center', justifyContent: 'center'
  },
  listContainer: {
    flex: 1,
    flexDirection: 'row',
    margin: 10,
    padding: 5,
    justifyContent: 'center',
  },
  headerTitle:{
    color: 'red', 
    fontSize: 20, 
    fontWeight: 'bold'
  },
  inputStyle:{
    color:'white'
  },
  description:{
    fontSize: 16,
    color: "black", 
    marginTop: 10, 
    fontStyle: "italic"
  },
  title:{
      fontSize: 20,
      color: "black",
      fontWeight: 'bold'
  },
  textContainer:{
    flexDirection:'column'
  },
  listView:{
    paddingVertical: 15,
    paddingHorizontal: 10,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "90%",
    backgroundColor: 'lightgray',
    borderRadius: 5,
    borderBottomWidth:3,
    borderBottomColor: 'red',
  },
};
export const styles = StyleSheet.create(TodoStyles);
