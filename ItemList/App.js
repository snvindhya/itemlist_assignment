import React from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import { persistStore, persistReducer } from 'redux-persist'
import ItemList from './component/ItemListApp';
import rootReducer from './Redux/reducers/rootReducer'
import { PersistGate } from 'redux-persist/integration/react'
import AsyncStorage from '@react-native-async-storage/async-storage';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
}
 
const persistedReducer = persistReducer(persistConfig, rootReducer)

let store = createStore(persistedReducer)
let persistor = persistStore(store)


const App = () => (
  <Provider store={store}>
  <PersistGate loading={null} persistor={persistor}>
  <ItemList/>
  </PersistGate>
</Provider>
);

export default App;
